/*
    SmartMatrix Features Demo - Louis Beaudoin (Pixelmatix)
    This example code is released into the public domain
*/

// #include <SmartLEDShieldV4.h>  // comment out this line for if you're not using SmartLED Shield V4 hardware (this line needs to be before #include <SmartMatrix3.h>)
#include <SmartMatrix3.h>

#define COLOR_DEPTH 24                  // known working: 24, 48 - If the sketch uses type `rgb24` directly, COLOR_DEPTH must be 24
const uint8_t kMatrixWidth = 32;        // known working: 32, 64, 96, 128
const uint8_t kMatrixHeight = 16;       // known working: 16, 32, 48, 64
const uint8_t kRefreshDepth = 36;       // known working: 24, 36, 48
const uint8_t kDmaBufferRows = 4;       // known working: 2-4, use 2 to save memory, more to keep from dropping frames and automatically lowering refresh rate
// const uint8_t kPanelType = SMARTMATRIX_HUB75_32ROW_MOD16SCAN; // use SMARTMATRIX_HUB75_16ROW_MOD8SCAN for common 16x32 panels, or use SMARTMATRIX_HUB75_64ROW_MOD32SCAN for common 64x64 panels
const uint8_t kPanelType = SMARTMATRIX_HUB75_16ROW_MOD8SCAN;
const uint8_t kMatrixOptions = (SMARTMATRIX_OPTIONS_NONE);      // see http://docs.pixelmatix.com/SmartMatrix for options
const uint8_t kBackgroundLayerOptions = (SM_BACKGROUND_OPTIONS_NONE);
const uint8_t kScrollingLayerOptions = (SM_SCROLLING_OPTIONS_NONE);
const uint8_t kIndexedLayerOptions = (SM_INDEXED_OPTIONS_NONE);

SMARTMATRIX_ALLOCATE_BUFFERS(matrix, kMatrixWidth, kMatrixHeight, kRefreshDepth, kDmaBufferRows, kPanelType, kMatrixOptions);
SMARTMATRIX_ALLOCATE_BACKGROUND_LAYER(backgroundLayer, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kBackgroundLayerOptions);
SMARTMATRIX_ALLOCATE_SCROLLING_LAYER(scrollingLayer, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kScrollingLayerOptions);
SMARTMATRIX_ALLOCATE_INDEXED_LAYER(indexedLayer, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kIndexedLayerOptions);

const int defaultBrightness = (100*255)/100;    // full (100%) brightness
//const int defaultBrightness = (15*255)/100;    // dim: 15% brightness
const int defaultScrollOffset = 6;
const rgb24 defaultBackgroundColor = {0x40, 0, 0};


// the setup() method runs once, when the sketch starts
void setup() {
  // Serial.begin(9600);

  matrix.addLayer(&backgroundLayer); 
  matrix.begin();

  matrix.setBrightness(defaultBrightness);

  backgroundLayer.enableColorCorrection(true);
}


// the loop() method runs over and over again,
// as long as the board has power
void loop() {
  unsigned long currentMillis;
  const uint transitionTime = 3000;
  int x0, y0;
  rgb24 color;

  currentMillis = millis();
  backgroundLayer.fillScreen({0, 0, 0});
        
  color.red = 0;
  color.green = 255;
  color.blue = 0;
  x0 = 0;
  y0 = 0;
  backgroundLayer.drawPixel(x0, y0, color);
     
  color.red = 255;
  color.green = 0;
  color.blue = 0;
  x0 = 0;
  y0 = 15;
  backgroundLayer.drawPixel(x0, y0, color);

  color.red = 0;
  color.green = 0;
  color.blue = 255;
  x0 = 31;
  y0 = 0;
  backgroundLayer.drawPixel(x0, y0, color);

  color.red = 255;
  color.green = 255;
  color.blue = 255;
  x0 = 31;
  y0 = 15;
  backgroundLayer.drawPixel(x0, y0, color);
        
  backgroundLayer.swapBuffers();

  while (millis() - currentMillis < transitionTime) {
  }
}
